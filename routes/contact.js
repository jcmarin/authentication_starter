const router = require('express').Router();
const verify = require('./verifyToken');
const Contact = require('../model/Contact');

router.get('/all',verify, async (req,res)=>{
    const contactList = await Contact.find();

    res.status(200).json(contactList);
});



module.exports = router;
