const router = require('express').Router();
const User = require('../model/User');
const jwt = require('jsonwebtoken');

const { registerValidation, loginValidation } = require('../validation');

const bcrypt = require('bcryptjs');


//Routes
router.post('/register', async (req,res)=>{
//LETS VALIDATE THE DATA BEFORE WE CREATE A USER. 
const { error } = registerValidation(req.body);

    if (error) return res.status(400).json({error: error.details[0].message});

    //Checking if user is in database already. 
    const emailExist = await User.findOne({email: req.body.email});
    if (emailExist) return res.status(422).json({error: 'email exists already'});

    //So far it is a valid user so lets hash the password. 
    const salt = await bcrypt.genSalt(10);
    console.log(salt);
    const hashPassword = await bcrypt.hash(req.body.password, salt);

    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hashPassword
    });

    try{
        const savedUser = await user.save();
        res.status(201).json({status: 'User Accepted', userCreated:{id: savedUser._id,email: savedUser.email}});
    }catch(err){
        res.send(err);
    }
   
});

router.post('/login', async (req,res)=>{
     //LETS VALIDATE THE DATA BEFORE WE LOGIN. 
    const { error } = loginValidation(req.body);   
    if (error) return res.status(400).json({error: error.details[0].message});
    
    //Checking if user is in database already. 
    const login_user = await User.findOne({email: req.body.email});
    if (!login_user) return res.status(404).json({error: 'Email or password is wrong.'});

    //Check if password is correct.
    const validPass = await bcrypt.compare(req.body.password, login_user.password);

    if(!validPass) return res.status(404).json({error: 'Email or password is wrong.'});
    //Create and assign token.
    console.log(process.env.TOKEN_SECRET);
    
    const token = jwt.sign({_id: login_user._id, app: 'jwt-security'}, process.env.TOKEN_SECRET);
    res.setHeader('auth-token',token);
    res.json({ token, id: login_user._id });
   






    res.status(201).json({msg: 'Logged in.'});
    
});


module.exports = router;