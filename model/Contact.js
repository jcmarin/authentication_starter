const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    first_name: {
            type: String,
            required: true,
            min: 6,
            max: 60
        },
    mi_name: {
            type: String,
            required: true,
            min: 6,
            max: 60
        },
    last_name: {
            type: String,
            required: true,
            min: 6,
            max: 60
        },
    email: {
        type: String,
        required: true,
        min: 6,
        max: 255
    },
    password: {
        type: String,
        required: true,
        min: 8,
        max: 1024
    },
    prima: {
        type: Number,
        required: true,
        min: 0,
        max: 102400000
    },
    expDate: {
        type: Date,
        default: Date.now,
        required: true
    },
    contactHist: [String],
    notes: [String],
    phone1: {
        type: String,
        required: true,
        min: 7,
        max: 13
    },
    phone2: {
        type: String,
        min: 7,
        max: 13
    },
    phone3: {
        type: String,
        min: 7,
        max: 12
    },
    date: {
        type: Date,
        default: Date.now
    }
});
 
module.exports = mongoose.model('Contact', userSchema);
