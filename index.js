const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
const authRoute = require('./routes/auth');
const postContact = require('./routes/contact');

const app = express();

//Connect to db 5oeI0IJb2UfI4zE7
mongoose.connect(process.env.DB_CONNECT,
{useNewUrlParser: true , useUnifiedTopology: true }, 
()=>{
    console.log('connected to db');
});

//Middleware
app.use(express.json());


//Route middlewares
app.use('/api/user', authRoute);
app.use('/',postContact);


app.listen(3000,console.log('Server listening on 3000'));